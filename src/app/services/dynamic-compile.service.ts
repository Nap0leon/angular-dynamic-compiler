import { Injectable, Injector, Compiler, NgModule, Component, ViewContainerRef } from '@angular/core';
import { DynamicModuleList } from './dynamic-module';

@Injectable({
  providedIn: 'root'
})
export class DynamicCompileService {

  constructor(private injector: Injector,
              private compiler: Compiler) { }

  createComponent(componentDefinition: string, viewContainerRef: ViewContainerRef) {
    const template = componentDefinition;
    const tempComponent = Component({ template })(class { });
    const tempModule = NgModule({ declarations: [tempComponent], imports: DynamicModuleList })(class { });

    this.compiler.compileModuleAndAllComponentsAsync(tempModule).then((factories) => {
      const factory = factories.componentFactories[0];
      const componentReference = factory.create(this.injector, [], null);
      componentReference.instance.name = 'dynamic';

      viewContainerRef.insert(componentReference.hostView);
    });
  }
}
