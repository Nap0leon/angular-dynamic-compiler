import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicCompileService } from './services/dynamic-compile.service';
import { DynamicCompileDirective } from './directives/dynamic-compile.directive';
import '@angular/compiler';

@NgModule({
  declarations: [
    AppComponent,
    DynamicCompileDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [DynamicCompileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
