import { Directive, Input, AfterViewInit, ViewContainerRef } from '@angular/core';
import { DynamicCompileService } from '../services/dynamic-compile.service';

@Directive({
  selector: '[appDynamicCompile]'
})
export class DynamicCompileDirective implements AfterViewInit {

  @Input('appDynamicCompile') dynamic: string;
  constructor(private el: ViewContainerRef,
              public dynamicCompileService: DynamicCompileService) {

  }

  ngAfterViewInit(): void {
    this.dynamicCompileService.createComponent(this.dynamic, this.el);
  }
}
